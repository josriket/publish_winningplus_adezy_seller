
/* Ajax calling by form handle */
function ajaxSubmitForm(event, form, submitBtnId, callBackMethod) {
    try{
        event.preventDefault();

        var btn = null;
        if (submitBtnId != null) {
            btn = $("#" + submitBtnId).ladda();
            btn.ladda("start");
        }


        var jForm = $(form)
        var url = jForm.attr("action");
        $.ajax({
            url: url,
            cache: false,
            type: jForm.attr("method"),
            dataType: "JSON",
            data: new FormData(jForm[0]),
            processData: false,
            contentType: false,
            success: function (data, status) {
                if(btn!=null) btn.ladda("stop");
                callBackMethod(data, status);
            },
            error: function (xhr, desc, err) {
                if (btn != null) btn.ladda("stop").ladda("stop");
                alert("Submit form fail");
            }
        });
    } catch (e) {
        alert('ajaxSubmitForm Error->' + e);
    }
}

function ajaxSubmitFormPartial(event, form, submitBtnId, callBackMethod) {
    try {
        event.preventDefault();

        var btn = null;
        if (submitBtnId != null) {
            btn = $("#" + submitBtnId).ladda();
            btn.ladda("start");
        }


        var jForm = $(form)
        var url = jForm.attr("action");
        $.ajax({
            url: url,
            cache: false,
            type: jForm.attr("method"),
            data: new FormData(jForm[0]),
            processData: false,
            contentType: false,
            success: function (data, status) {
                if (btn != null) btn.ladda("stop");
                callBackMethod(data);
            },
            error: function (xhr, desc, err) {
                if (btn != null) btn.ladda("stop").ladda("stop");
                alert("Submit form fail");
            }
        });
    } catch (e) {
        alert('ajaxSubmitForm Error->' + e);
    }
}

function ajaxPartialLoad(url, targetElement, callBackMethod) {
    try {
        if (targetElement != null) {
            $("#" + targetElement).html("Please wait...");
        }
	     $.ajax({
	        url: url,
	        cache: false,
	        type: 'get',
	        success: function (data) {
	            $("#" + targetElement).html(data);
	    	    callBackMethod(data);
	        }
	     });
    } catch (e) {
        alert('ajaxPartialLoad Error->' + e);
    }
}

function ajaxUrlRequest(url,callBackMethod){
    try {
	    $.ajax({
	        url: url,
	        cache: false,
	        type: 'get',
	        success: function(data) {
	    	    callBackMethod(data);
	        }
	    });
    } catch (e) {
        alert('ajaxPartialLoad Error->' + e);
    }
}